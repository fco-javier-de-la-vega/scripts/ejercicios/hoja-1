Algoritmo SeleccionSimple
	//Realizar un programa que lee un caracter y te indica si es una a o una b, en caso contrario te debe indicar que no es ninguno de los dos caracteres
	c1 <- "a"
	Escribir 'introduce un caracter:'
	Leer c1
	Si c1="a" Entonces
		Escribir "El caracter es una a"
	SiNo
		Si c1="b" Entonces
			Escribir "El caracter es una b"
		SiNo
			Escribir "El caracter no es ni a ni b."
		Fin Si
	Fin Si
FinAlgoritmo
